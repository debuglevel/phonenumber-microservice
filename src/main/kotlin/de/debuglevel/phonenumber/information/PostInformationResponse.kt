package de.debuglevel.phonenumber.information

data class PostInformationResponse(
    @Deprecated("Use originalPhonenumber instead", replaceWith = ReplaceWith("this.originalPhonenumber"))
    val phonenumber: String,
    val originalPhonenumber: String,

    @Deprecated("Use TAPI format instead", replaceWith = ReplaceWith("this.tapi"))
    val formattedPhonenumber: String? = null,
    val e164: String? = null,
    val tapi: String? = null,

    val location: String? = null,
    val regionCode: String? = null,
    val type: String? = null,

    val error: String? = null
)