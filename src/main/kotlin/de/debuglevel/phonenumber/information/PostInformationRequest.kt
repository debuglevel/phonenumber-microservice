package de.debuglevel.phonenumber.information

data class PostInformationRequest(val phonenumber: String)