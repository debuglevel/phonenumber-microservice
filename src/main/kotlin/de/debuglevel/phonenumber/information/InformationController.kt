package de.debuglevel.phonenumber.information

import de.debuglevel.phonenumber.InvalidPhonenumberException
import de.debuglevel.phonenumber.format.FormatService
import de.debuglevel.phonenumber.geocode.GeocodeService
import de.debuglevel.phonenumber.parser.PhonenumberService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import mu.KotlinLogging

@Controller("/information")
class InformationController(
    private val geocodeService: GeocodeService,
    private val formatService: FormatService,
    private val phonenumberService: PhonenumberService,
) {
    private val logger = KotlinLogging.logger {}

    @Post("/")
    fun postOne(postInformationRequest: PostInformationRequest): HttpResponse<PostInformationResponse> {
        logger.debug("Called postOne($postInformationRequest)")

        val phoneNumber = try {
            phonenumberService.parseAndValidate(postInformationRequest.phonenumber)
        } catch (e: InvalidPhonenumberException) {
            val errorResponse = PostInformationResponse(
                postInformationRequest.phonenumber,
                postInformationRequest.phonenumber,
                error = "the phone number is invalid"
            )
            return HttpResponse.badRequest(errorResponse)
        }

        val formatResponse = PostInformationResponse(
            phonenumber = postInformationRequest.phonenumber,
            originalPhonenumber = postInformationRequest.phonenumber,

            formattedPhonenumber = formatService.formatTapi(phoneNumber),
            e164 = formatService.formatE164(phoneNumber),
            tapi = formatService.formatTapi(phoneNumber),

            location = geocodeService.geocode(phoneNumber).location,
            regionCode = phonenumberService.getRegionCode(phoneNumber),
            type = phonenumberService.getNumberType(phoneNumber).name,
        )

        return HttpResponse.ok(formatResponse)
    }
}