package de.debuglevel.phonenumber.geocode

@Deprecated("Use Information endpoint instead")
data class GeocodeRequest(val phonenumber: String)