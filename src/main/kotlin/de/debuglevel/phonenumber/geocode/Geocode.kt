package de.debuglevel.phonenumber.geocode

import com.google.i18n.phonenumbers.PhoneNumberUtil

data class Geocode(
    val location: String,
    val regionCode: String?,
    val type: PhoneNumberUtil.PhoneNumberType,
)