package de.debuglevel.phonenumber.geocode

import com.google.i18n.phonenumbers.Phonenumber
import com.google.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder
import de.debuglevel.phonenumber.parser.PhonenumberService
import jakarta.inject.Singleton
import mu.KotlinLogging
import java.util.*

@Singleton
class GeocodeService(
    private val phonenumberService: PhonenumberService
) {
    private val logger = KotlinLogging.logger {}

    private val geocoder: PhoneNumberOfflineGeocoder = PhoneNumberOfflineGeocoder.getInstance()

    /**
     * Geocodes the [Phonenumber.PhoneNumber] into a [Geocode].
     */
    fun geocode(phoneNumber: Phonenumber.PhoneNumber): Geocode {
        logger.debug { "Geocoding '$phoneNumber'..." }

        val geocode = Geocode(
            location = geocoder.getDescriptionForNumber(phoneNumber, Locale.GERMAN),
            regionCode = phonenumberService.getRegionCode(phoneNumber),
            type = phonenumberService.getNumberType(phoneNumber),
        )

        logger.debug { "Geocoded '$phoneNumber': '$geocode'" }
        return geocode
    }
}