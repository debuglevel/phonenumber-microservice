package de.debuglevel.phonenumber.geocode

import de.debuglevel.phonenumber.InvalidPhonenumberException
import de.debuglevel.phonenumber.parser.PhonenumberService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import mu.KotlinLogging

@Deprecated("Use Information endpoint instead")
@Controller("/geocode")
class GeocodeController(
    private val geocodeService: GeocodeService,
    private val phonenumberService: PhonenumberService,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Geocode a phone number
     * @param geocodeRequest original phone number
     * @return Geocode
     */
    @Post("/")
    fun postOne(geocodeRequest: GeocodeRequest): HttpResponse<GeocodeResponse> {
        logger.debug("Called postOne($geocodeRequest)")

        val geocode = try {
            val phoneNumber = phonenumberService.parseAndValidate(geocodeRequest.phonenumber)
            geocodeService.geocode(phoneNumber)
        } catch (e: InvalidPhonenumberException) {
            val errorResponse = GeocodeResponse(
                geocodeRequest.phonenumber,
                error = "the phone number is invalid"
            )
            return HttpResponse.badRequest(errorResponse)
        }

        val formatResponse = GeocodeResponse(
            geocodeRequest.phonenumber,
            geocode.location,
            geocode.regionCode,
            geocode.type.name
        )

        return HttpResponse.ok(formatResponse)
    }
}