package de.debuglevel.phonenumber.format

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import de.debuglevel.phonenumber.parser.PhonenumberService
import jakarta.inject.Singleton
import mu.KotlinLogging

@Singleton
class FormatService(
    private val phonenumberService: PhonenumberService
) {
    private val logger = KotlinLogging.logger {}

    private val phonenumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()

    /**
     * Formats the [Phonenumber.PhoneNumber] into an E.164 formatted phone number (i.e. `+49 151 1234567`).
     */
    fun formatE164(phoneNumber: Phonenumber.PhoneNumber): String {
        logger.debug { "Formatting '$phoneNumber' to E.164..." }

        val e164: String = phonenumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164)

        logger.debug { "Formatted '$phoneNumber to E.164: $e164" }
        return e164
    }

    /**
     * Formats the [Phonenumber.PhoneNumber] into a Microsoft TAPI compliant phone number (i.e. `+49 (151) 1234567`).
     */
    fun formatTapi(phoneNumber: Phonenumber.PhoneNumber): String {
        logger.debug { "Formatting '$phoneNumber' to TAPI..." }

        val international: String = phonenumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
        val tapi = addBrackets(international)

        logger.debug { "Formatted '$phoneNumber to TAPI: $tapi" }
        return tapi
    }

    /**
     * Adds brackets around the city prefix of a phone number,
     * so that a phone number like `+49 951 12345678` gets formatted to `+49 (951) 12345678`.
     */
    private fun addBrackets(phonenumber: String): String {
        logger.debug { "Adding brackets to phone number '$phonenumber'..." }

        val parts = phonenumber.split(' ')
        val phonenumberWithBrackets =
            ("${parts[0]} (${parts[1]}) " +
                    parts.subList(2, parts.size).joinToString(" "))
                .trim()

        logger.debug { "Added brackets to phone number '$phonenumber': '$phonenumberWithBrackets'" }
        return phonenumberWithBrackets
    }
}