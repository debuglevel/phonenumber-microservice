package de.debuglevel.phonenumber.format

@Deprecated("Use Information endpoint instead")
data class PostFormatResponse(
    val phonenumber: String,
    val formattedPhonenumber: String? = null,
    val error: String? = null
)