package de.debuglevel.phonenumber.format

import de.debuglevel.phonenumber.parser.PhonenumberService
import jakarta.inject.Singleton
import mu.KotlinLogging

@Singleton
class FormatEndpoint(
    private val formatService: FormatService,
    private val phonenumberService: PhonenumberService,
) : FormatServiceGrpcKt.FormatServiceCoroutineImplBase() {
    private val logger = KotlinLogging.logger {}

    override suspend fun format(request: FormatRequest): FormatReply {
        logger.debug("Called format($request)")

        val phoneNumber = phonenumberService.parseAndValidate(request.phonenumber)

        val formatReply = FormatReply.newBuilder().apply {
            e164 = formatService.formatE164(phoneNumber)
            tapi = formatService.formatTapi(phoneNumber)
        }.build()

        logger.debug("Called format($request): $formatReply")
        return formatReply
    }
}