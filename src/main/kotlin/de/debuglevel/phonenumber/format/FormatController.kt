package de.debuglevel.phonenumber.format

import de.debuglevel.phonenumber.InvalidPhonenumberException
import de.debuglevel.phonenumber.parser.PhonenumberService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import mu.KotlinLogging

@Deprecated("Use Information endpoint instead")
@Controller("/format")
class FormatController(
    private val formatService: FormatService,
    private val phonenumberService: PhonenumberService,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Format a phone number
     * @param postFormatRequest original phone number
     * @return phone number
     */
    @Post("/")
    fun postOne(postFormatRequest: PostFormatRequest): HttpResponse<PostFormatResponse> {
        logger.debug("Called postOne($postFormatRequest)")

        val formattedPhonenumber = try {
            val phoneNumber = phonenumberService.parseAndValidate(postFormatRequest.phonenumber)
            formatService.formatE164(phoneNumber)
        } catch (e: InvalidPhonenumberException) {
            val errorResponse = PostFormatResponse(
                postFormatRequest.phonenumber,
                error = "the phone number is invalid"
            )
            return HttpResponse.badRequest(errorResponse)
        }

        val postFormatResponse = PostFormatResponse(
            postFormatRequest.phonenumber,
            formattedPhonenumber
        )

        return HttpResponse.ok(postFormatResponse)
    }
}