package de.debuglevel.phonenumber.format

@Deprecated("Use Information endpoint instead")
data class PostFormatRequest(val phonenumber: String)