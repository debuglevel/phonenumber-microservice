package de.debuglevel.phonenumber.parser

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import de.debuglevel.phonenumber.InvalidPhonenumberException
import io.micronaut.context.annotation.Property
import jakarta.inject.Singleton
import mu.KotlinLogging

@Singleton
class PhonenumberService(
    @Property(name = "app.default-region") private val defaultRegion: String,
) {
    private val logger = KotlinLogging.logger {}

    private val phonenumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()

    /**
     * Parses a [phonenumber] into a [Phonenumber.PhoneNumber] and checks if it is valid.
     */
    fun parseAndValidate(phonenumber: String): Phonenumber.PhoneNumber {
        logger.debug { "Parsing and validating phone number '$phonenumber' with default region '$defaultRegion'..." }

        val parsedPhoneNumber: Phonenumber.PhoneNumber = try {
            val parsedPhonenumber: Phonenumber.PhoneNumber = phonenumberUtil.parse(phonenumber, defaultRegion)

            if (!phonenumberUtil.isValidNumber(parsedPhonenumber)) {
                throw InvalidPhonenumberException(phonenumber)
            }

            parsedPhonenumber
        } catch (e: NumberParseException) {
            throw InvalidPhonenumberException(phonenumber, e)
        }

        logger.debug { "Parsed and validated phone number '$phonenumber' with default region '$defaultRegion': $parsedPhoneNumber" }
        return parsedPhoneNumber
    }

    /**
     * Returns the region where the [Phonenumber.PhoneNumber] is from; `null` if no region matches.
     */
    fun getRegionCode(phonenumber: Phonenumber.PhoneNumber): String? {
        logger.debug { "Getting region code of '$phonenumber'..." }

        val regionCode: String? = phonenumberUtil.getRegionCodeForNumber(phonenumber)

        logger.debug { "Got region code of '$phonenumber': $regionCode" }
        return regionCode
    }

    /**
     * Returns the type of the [Phonenumber.PhoneNumber].
     */
    fun getNumberType(phonenumber: Phonenumber.PhoneNumber): PhoneNumberUtil.PhoneNumberType {
        logger.debug { "Getting number type of '$phonenumber'..." }

        val numberType: PhoneNumberUtil.PhoneNumberType = phonenumberUtil.getNumberType(phonenumber)

        logger.debug { "Got number type of '$phonenumber': $numberType" }
        return numberType
    }
}