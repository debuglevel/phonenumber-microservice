package de.debuglevel.phonenumber.geocode


import de.debuglevel.phonenumber.parser.PhonenumberService
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GeocodeServiceTests {

    @Inject
    lateinit var phonenumberService: PhonenumberService

    @Inject
    lateinit var geocodeService: GeocodeService

    fun validPhonenumberProvider() = TestDataProvider.validPhonenumberProvider()
    fun invalidPhonenumberProvider() = TestDataProvider.invalidPhonenumberProvider()

    @ParameterizedTest
    @MethodSource("validPhonenumberProvider")
    fun `geocode valid phone numbers`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange
        val phoneNumber = phonenumberService.parseAndValidate(testData.value)

        // Act
        val geocode = geocodeService.geocode(phoneNumber)

        // Assert
        assertThat(geocode.location).isEqualTo(testData.expectedLocation)
        assertThat(geocode.regionCode).isEqualTo(testData.expectedRegionCode)
        assertThat(geocode.type.name).isEqualTo(testData.expectedType)
    }
}