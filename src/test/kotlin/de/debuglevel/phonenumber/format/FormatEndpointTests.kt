package de.debuglevel.phonenumber.format

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource


@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FormatEndpointTests {
    @Inject
    lateinit var formatBlockingStub: FormatServiceGrpc.FormatServiceBlockingStub

    fun validPhonenumberProvider() = TestDataProvider.validPhonenumberProvider()
    fun invalidPhonenumberProvider() = TestDataProvider.invalidPhonenumberProvider()

    @ParameterizedTest
    @MethodSource("validPhonenumberProvider")
    fun `format valid phone numbers`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange
        val postFormatRequest = FormatRequest.newBuilder().apply {
            phonenumber = testData.value
        }.build()

        // Act
        val formatReply = formatBlockingStub.format(postFormatRequest)

        // Assert
        Assertions.assertThat(formatReply.tapi).isEqualTo(testData.expectedTapi)
        Assertions.assertThat(formatReply.e164).isEqualTo(testData.expectedE164)
    }

    // do not test "invalid" names (i.e. "" and " ") as their HTTP call would translate to "/greetings/" which then returns the getList() stuff
//    @ParameterizedTest
//    @MethodSource("invalidNameProvider")
//    fun `greet invalid names`(testData: TestDataProvider.NameTestData) {
//        // Arrange
//
//        // Act
//        val greeting = httpClient.toBlocking().retrieve("/${testData.name}?language=${testData.language}")
//
//        // Assert
//        Assertions.assertThat(greeting).contains("500")
//    }
//
//    fun invalidNameProvider(): Stream<TestDataProvider.NameTestData> {
//        return TestDataProvider.invalidNameProvider()
//    }
}