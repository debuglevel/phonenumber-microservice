package de.debuglevel.phonenumber.format

import io.grpc.ManagedChannel
import io.micronaut.context.annotation.Bean
import io.micronaut.context.annotation.Factory
import io.micronaut.grpc.annotation.GrpcChannel
import io.micronaut.grpc.server.GrpcServerChannel

@Factory
class FormatGrpcClient {
    @Bean
    fun blockingStub(@GrpcChannel(GrpcServerChannel.NAME) channel: ManagedChannel): FormatServiceGrpc.FormatServiceBlockingStub {
        return FormatServiceGrpc.newBlockingStub(channel)
    }
}