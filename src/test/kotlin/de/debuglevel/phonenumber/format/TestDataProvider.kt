package de.debuglevel.phonenumber.format

import java.util.stream.Stream

object TestDataProvider {
    fun validPhonenumberProvider() = Stream.of(
        PhonenumberTestData(
            value = "(09 51) 2 08 50-11",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "0951 2085011",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "0951/2085011",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "0951 / 2085011",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "00499512085011",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "+499512085011",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(
            value = "0951/20850-11",
            expectedTapi = "+49 (951) 2085011",
            expectedE164 = "+499512085011"
        ),
        PhonenumberTestData(value = "+49742512345", expectedTapi = "+49 (7425) 12345", expectedE164 = "+49742512345"),
        PhonenumberTestData(value = "0742512345", expectedTapi = "+49 (7425) 12345", expectedE164 = "+49742512345"),
        PhonenumberTestData(value = "07425/12345", expectedTapi = "+49 (7425) 12345", expectedE164 = "+49742512345"),
        PhonenumberTestData(value = "07425-12345", expectedTapi = "+49 (7425) 12345", expectedE164 = "+49742512345"),

        PhonenumberTestData(
            value = "+49 (9133) 7684800",
            expectedTapi = "+49 (9133) 7684800",
            expectedE164 = "+4991337684800"
        ),
        PhonenumberTestData(
            value = "+49 (170) 9046050",
            expectedTapi = "+49 (170) 9046050",
            expectedE164 = "+491709046050"
        ),
        PhonenumberTestData(
            value = "+49 (176) 43308420",
            expectedTapi = "+49 (176) 43308420",
            expectedE164 = "+4917643308420"
        ),
        PhonenumberTestData(
            value = "+49 (961) 4707030",
            expectedTapi = "+49 (961) 4707030",
            expectedE164 = "+499614707030"
        ),
        PhonenumberTestData(
            value = "+49 (176) 32777370",
            expectedTapi = "+49 (176) 32777370",
            expectedE164 = "+4917632777370"
        ),
        PhonenumberTestData(
            value = "+49 (941) 7000520",
            expectedTapi = "+49 (941) 7000520",
            expectedE164 = "+499417000520"
        ),
        PhonenumberTestData(
            value = "+49 (1577) 3838050",
            expectedTapi = "+49 (1577) 3838050",
            expectedE164 = "+4915773838050"
        ),
        PhonenumberTestData(
            value = "+43 (699) 19496870",
            expectedTapi = "+43 (699) 19496870",
            expectedE164 = "+4369919496870"
        ),
        PhonenumberTestData(
            value = "+49 (89) 98295710",
            expectedTapi = "+49 (89) 98295710",
            expectedE164 = "+498998295710"
        ),
        PhonenumberTestData(
            value = "+49 (172) 8591990",
            expectedTapi = "+49 (172) 8591990",
            expectedE164 = "+491728591990"
        ),
        PhonenumberTestData(
            value = "+49 (851) 75681330",
            expectedTapi = "+49 (851) 75681330",
            expectedE164 = "+4985175681330"
        ),
        PhonenumberTestData(
            value = "+49 (9701) 907830",
            expectedTapi = "+49 (9701) 907830",
            expectedE164 = "+499701907830"
        ),
        PhonenumberTestData(
            value = "+49 (174) 7492500",
            expectedTapi = "+49 (174) 7492500",
            expectedE164 = "+491747492500"
        ),
        PhonenumberTestData(
            value = "+49 (1525) 3012250",
            expectedTapi = "+49 (1525) 3012250",
            expectedE164 = "+4915253012250"
        ),
        PhonenumberTestData(
            value = "+49 (1516) 1116120",
            expectedTapi = "+49 (1516) 1116120",
            expectedE164 = "+4915161116120"
        ),
        PhonenumberTestData(
            value = "+49 (1573) 0093850",
            expectedTapi = "+49 (1573) 0093850",
            expectedE164 = "+4915730093850"
        ),
        PhonenumberTestData(
            value = "+49 (6402) 508420",
            expectedTapi = "+49 (6402) 508420",
            expectedE164 = "+496402508420"
        ),
        PhonenumberTestData(
            value = "+49 (176) 55447770",
            expectedTapi = "+49 (176) 55447770",
            expectedE164 = "+4917655447770"
        ),
        PhonenumberTestData(
            value = "+49 (176) 66836960",
            expectedTapi = "+49 (176) 66836960",
            expectedE164 = "+4917666836960"
        ),
        PhonenumberTestData(
            value = "+49 (89) 99735659",
            expectedTapi = "+49 (89) 99735659",
            expectedE164 = "+498999735659"
        ),
        PhonenumberTestData(
            value = "+49 (9131) 7532300",
            expectedTapi = "+49 (9131) 7532300",
            expectedE164 = "+4991317532300"
        ),
        PhonenumberTestData(
            value = "+49 (8158) 2433009",
            expectedTapi = "+49 (8158) 2433009",
            expectedE164 = "+4981582433009"
        ),
        PhonenumberTestData(
            value = "+49 (7525) 932730",
            expectedTapi = "+49 (7525) 932730",
            expectedE164 = "+497525932730"
        ),
        PhonenumberTestData(
            value = "+49 (179) 2182320",
            expectedTapi = "+49 (179) 2182320",
            expectedE164 = "+491792182320"
        ),
        PhonenumberTestData(
            value = "+49 (9197) 627260",
            expectedTapi = "+49 (9197) 627260",
            expectedE164 = "+499197627260"
        ),
        PhonenumberTestData(
            value = "+49 (176) 27054530",
            expectedTapi = "+49 (176) 27054530",
            expectedE164 = "+4917627054530"
        ),
        PhonenumberTestData(
            value = "+49 (911) 5984910",
            expectedTapi = "+49 (911) 5984910",
            expectedE164 = "+499115984910"
        ),
        PhonenumberTestData(
            value = "+49 (176) 54212490",
            expectedTapi = "+49 (176) 54212490",
            expectedE164 = "+4917654212490"
        ),
        PhonenumberTestData(
            value = "+49 (89) 55988120",
            expectedTapi = "+49 (89) 55988120",
            expectedE164 = "+498955988120"
        ),
        PhonenumberTestData(
            value = "+49 (89) 5806910",
            expectedTapi = "+49 (89) 5806910",
            expectedE164 = "+49895806910"
        ),
        PhonenumberTestData(
            value = "+49 (55) 13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49 (171) 6190520",
            expectedTapi = "+49 (171) 6190520",
            expectedE164 = "+491716190520"
        ),
        PhonenumberTestData(
            value = "+49 (160) 3190050",
            expectedTapi = "+49 (160) 3190050",
            expectedE164 = "+491603190050"
        ),
        PhonenumberTestData(
            value = "+43 (699) 13018809",
            expectedTapi = "+43 (699) 13018809",
            expectedE164 = "+4369913018809"
        ),
        PhonenumberTestData(
            value = "+49 (176) 78159310",
            expectedTapi = "+49 (176) 78159310",
            expectedE164 = "+4917678159310"
        ),
        PhonenumberTestData(
            value = "+49 (174) 8028690",
            expectedTapi = "+49 (174) 8028690",
            expectedE164 = "+491748028690"
        ),
        PhonenumberTestData(
            value = "+49 (621) 1812100",
            expectedTapi = "+49 (621) 1812100",
            expectedE164 = "+496211812100"
        ),
        PhonenumberTestData(
            value = "+49 (1520) 8679760",
            expectedTapi = "+49 (1520) 8679760",
            expectedE164 = "+4915208679760"
        ),
        PhonenumberTestData(
            value = "+49 (931) 4652080",
            expectedTapi = "+49 (931) 4652080",
            expectedE164 = "+499314652080"
        ),
        PhonenumberTestData(
            value = "+49 (176) 99816390",
            expectedTapi = "+49 (176) 99816390",
            expectedE164 = "+4917699816390"
        ),
        PhonenumberTestData(
            value = "+49 (172) 9480139",
            expectedTapi = "+49 (172) 9480139",
            expectedE164 = "+491729480139"
        ),
        PhonenumberTestData(
            value = "+49 (176) 55391450",
            expectedTapi = "+49 (176) 55391450",
            expectedE164 = "+4917655391450"
        ),
        PhonenumberTestData(
            value = "+49 (162) 2456460",
            expectedTapi = "+49 (162) 2456460",
            expectedE164 = "+491622456460"
        ),
        PhonenumberTestData(
            value = "+49 (174) 3028760",
            expectedTapi = "+49 (174) 3028760",
            expectedE164 = "+491743028760"
        ),
        PhonenumberTestData(
            value = "+49 (8131) 1060",
            expectedTapi = "+49 (8131) 1060",
            expectedE164 = "+4981311060"
        ),

        PhonenumberTestData(
            value = "+49 (55) 13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+495513965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49(55) 13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49 (55)13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49(55)13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49-55-13965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "00495513965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 3965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551/3965270",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551/39652-70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "+49 0551/39652-70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 /39652-70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551/ 39652-70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 / 39652-70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 / 39652 -70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 / 39652 - 70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),
        PhonenumberTestData(
            value = "0551 / 396 52 - 70",
            expectedTapi = "+49 (551) 3965270",
            expectedE164 = "+495513965270"
        ),

        // T-Mobile
        PhonenumberTestData(
            value = "+49 1510 3028709",
            expectedTapi = "+49 (1510) 3028709",
            expectedE164 = "+4915103028709"
        ),
        PhonenumberTestData(
            value = "+49 1511 3028709",
            expectedTapi = "+49 (1511) 3028709",
            expectedE164 = "+4915113028709"
        ),
        PhonenumberTestData(
            value = "+49 1512 3028709",
            expectedTapi = "+49 (1512) 3028709",
            expectedE164 = "+4915123028709"
        ),
        PhonenumberTestData(
            value = "+49 1513 3028709",
            expectedTapi = "+49 (1513) 3028709",
            expectedE164 = "+4915133028709"
        ),
        PhonenumberTestData(
            value = "+49 1514 3028709",
            expectedTapi = "+49 (1514) 3028709",
            expectedE164 = "+4915143028709"
        ),
        PhonenumberTestData(
            value = "+49 1515 3028709",
            expectedTapi = "+49 (1515) 3028709",
            expectedE164 = "+4915153028709"
        ),
        PhonenumberTestData(
            value = "+49 1516 3028709",
            expectedTapi = "+49 (1516) 3028709",
            expectedE164 = "+4915163028709"
        ),
        PhonenumberTestData(
            value = "+49 1517 3028709",
            expectedTapi = "+49 (1517) 3028709",
            expectedE164 = "+4915173028709"
        ),
        PhonenumberTestData(
            value = "+49 1518 3028709",
            expectedTapi = "+49 (1518) 3028709",
            expectedE164 = "+4915183028709"
        ),
        PhonenumberTestData(
            value = "+49 1519 3028709",
            expectedTapi = "+49 (1519) 3028709",
            expectedE164 = "+4915193028709"
        ),
        PhonenumberTestData(
            value = "+49 160 3028760",
            expectedTapi = "+49 (160) 3028760",
            expectedE164 = "+491603028760"
        ),
        PhonenumberTestData(
            value = "+49 170 3028760",
            expectedTapi = "+49 (170) 3028760",
            expectedE164 = "+491703028760"
        ),
        PhonenumberTestData(
            value = "+49 171 3028760",
            expectedTapi = "+49 (171) 3028760",
            expectedE164 = "+491713028760"
        ),
        PhonenumberTestData(
            value = "+49 175 3028760",
            expectedTapi = "+49 (175) 3028760",
            expectedE164 = "+491753028760"
        ),

        // Vodafone
        PhonenumberTestData(
            value = "+49 1520 3028760",
            expectedTapi = "+49 (1520) 3028760",
            expectedE164 = "+4915203028760"
        ),
        PhonenumberTestData(
            value = "+49 1521 3028760",
            expectedTapi = "+49 (1521) 3028760",
            expectedE164 = "+4915213028760"
        ),
        PhonenumberTestData(
            value = "+49 1522 3028760",
            expectedTapi = "+49 (1522) 3028760",
            expectedE164 = "+4915223028760"
        ),
        PhonenumberTestData(
            value = "+49 1523 3028760",
            expectedTapi = "+49 (1523) 3028760",
            expectedE164 = "+4915233028760"
        ),
        PhonenumberTestData(
            value = "+49 1524 3028760",
            expectedTapi = "+49 (1524) 3028760",
            expectedE164 = "+4915243028760"
        ),
        PhonenumberTestData(
            value = "+49 1525 3028760",
            expectedTapi = "+49 (1525) 3028760",
            expectedE164 = "+4915253028760"
        ),
        PhonenumberTestData(
            value = "+49 1526 3028760",
            expectedTapi = "+49 (1526) 3028760",
            expectedE164 = "+4915263028760"
        ),
        PhonenumberTestData(
            value = "+49 1527 3028760",
            expectedTapi = "+49 (1527) 3028760",
            expectedE164 = "+4915273028760"
        ),
        PhonenumberTestData(
            value = "+49 1528 3028760",
            expectedTapi = "+49 (1528) 3028760",
            expectedE164 = "+4915283028760"
        ),
        PhonenumberTestData(
            value = "+49 1529 3028760",
            expectedTapi = "+49 (1529) 3028760",
            expectedE164 = "+4915293028760"
        ),

        // E-Plus
        PhonenumberTestData(
            value = "+49 1570 3028760",
            expectedTapi = "+49 (1570) 3028760",
            expectedE164 = "+4915703028760"
        ),
        PhonenumberTestData(
            value = "+49 1571 3028760",
            expectedTapi = "+49 (1571) 3028760",
            expectedE164 = "+4915713028760"
        ),
        PhonenumberTestData(
            value = "+49 1572 3028760",
            expectedTapi = "+49 (1572) 3028760",
            expectedE164 = "+4915723028760"
        ),
        PhonenumberTestData(
            value = "+49 1573 3028760",
            expectedTapi = "+49 (1573) 3028760",
            expectedE164 = "+4915733028760"
        ),
        PhonenumberTestData(
            value = "+49 1574 3028760",
            expectedTapi = "+49 (1574) 3028760",
            expectedE164 = "+4915743028760"
        ),
        PhonenumberTestData(
            value = "+49 1575 3028760",
            expectedTapi = "+49 (1575) 3028760",
            expectedE164 = "+4915753028760"
        ),
        PhonenumberTestData(
            value = "+49 1576 3028760",
            expectedTapi = "+49 (1576) 3028760",
            expectedE164 = "+4915763028760"
        ),
        PhonenumberTestData(
            value = "+49 1577 3028760",
            expectedTapi = "+49 (1577) 3028760",
            expectedE164 = "+4915773028760"
        ),
        PhonenumberTestData(
            value = "+49 1578 3028760",
            expectedTapi = "+49 (1578) 3028760",
            expectedE164 = "+4915783028760"
        ),
        PhonenumberTestData(
            value = "+49 1579 3028760",
            expectedTapi = "+49 (1579) 3028760",
            expectedE164 = "+4915793028760"
        ),
        PhonenumberTestData(
            value = "+49 163 3028760",
            expectedTapi = "+49 (163) 3028760",
            expectedE164 = "+491633028760"
        ),
        PhonenumberTestData(
            value = "+49 177 3028760",
            expectedTapi = "+49 (177) 3028760",
            expectedE164 = "+491773028760"
        ),
        PhonenumberTestData(
            value = "+49 178 3028760",
            expectedTapi = "+49 (178) 3028760",
            expectedE164 = "+491783028760"
        ),

        // O2
        PhonenumberTestData(
            value = "+49 1590 3028760",
            expectedTapi = "+49 (1590) 3028760",
            expectedE164 = "+4915903028760"
        ),
        PhonenumberTestData(
            value = "+49 176 3028760",
            expectedTapi = "+49 (176) 3028760",
            expectedE164 = "+491763028760"
        ),
        PhonenumberTestData(
            value = "+49 179 3028760",
            expectedTapi = "+49 (179) 3028760",
            expectedE164 = "+491793028760"
        ),

        // produces more groups than german numbers
        PhonenumberTestData(
            value = "+41 44 668 18 00",
            expectedTapi = "+41 (44) 668 18 00",
            expectedE164 = "+41446681800"
        ),
        PhonenumberTestData(
            value = "+41 446681800",
            expectedTapi = "+41 (44) 668 18 00",
            expectedE164 = "+41446681800"
        )
    )

    fun invalidPhonenumberProvider() = Stream.of(
        PhonenumberTestData(value = "1"),
        PhonenumberTestData(value = "+12"),
        PhonenumberTestData(value = "1/2"),
        PhonenumberTestData(value = "+1 2"),
        PhonenumberTestData(value = "foo"),

        // internal numbers
        PhonenumberTestData(value = "123"),
        PhonenumberTestData(value = "111"),
        PhonenumberTestData(value = "333"),
        PhonenumberTestData(value = "21"),
        PhonenumberTestData(value = "09"),

        // invalid numbers
        PhonenumberTestData(value = "+49(151)23299"),
        PhonenumberTestData(value = "015123299")
    )

    data class PhonenumberTestData(
        val value: String,
        val expectedTapi: String? = null,
        val expectedE164: String? = null,
    )
}