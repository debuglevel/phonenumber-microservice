package de.debuglevel.phonenumber.format


import de.debuglevel.phonenumber.parser.PhonenumberService
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FormatServiceTests {

    @Inject
    lateinit var phonenumberService: PhonenumberService

    @Inject
    lateinit var formatService: FormatService

    fun validPhonenumberProvider() = TestDataProvider.validPhonenumberProvider()
    fun invalidPhonenumberProvider() = TestDataProvider.invalidPhonenumberProvider()

    @ParameterizedTest
    @MethodSource("validPhonenumberProvider")
    fun `format phone numbers to E164`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange
        val phoneNumber = phonenumberService.parseAndValidate(testData.value)

        // Act
        val formattedPhonenumber = formatService.formatE164(phoneNumber)

        // Assert
        assertThat(formattedPhonenumber).isEqualTo(testData.expectedE164)
    }

    @ParameterizedTest
    @MethodSource("validPhonenumberProvider")
    fun `format phone numbers to TAPI`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange
        val phoneNumber = phonenumberService.parseAndValidate(testData.value)

        // Act
        val formattedPhonenumber = formatService.formatTapi(phoneNumber)

        // Assert
        assertThat(formattedPhonenumber).isEqualTo(testData.expectedTapi)
    }
}