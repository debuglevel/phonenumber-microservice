package de.debuglevel.phonenumber.parser


import de.debuglevel.phonenumber.InvalidPhonenumberException
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PhonenumberServiceTests {

    @Inject
    lateinit var phonenumberService: PhonenumberService

    fun validPhonenumberProvider() = TestDataProvider.validPhonenumberProvider()
    fun invalidPhonenumberProvider() = TestDataProvider.invalidPhonenumberProvider()

    @ParameterizedTest
    @MethodSource("validPhonenumberProvider")
    fun `parse valid phone numbers`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange

        // Act
        val phonenumber = phonenumberService.parseAndValidate(testData.value)

        // Assert
        assertThat(phonenumber.countryCode).isEqualTo(testData.expectedCountryCode)
        assertThat(phonenumber.nationalNumber).isEqualTo(testData.expectedNationalNumber)
    }

    @ParameterizedTest
    @MethodSource("invalidPhonenumberProvider")
    fun `parse invalid phone numbers`(testData: TestDataProvider.PhonenumberTestData) {
        // Arrange

        // Act

        // Assert
        assertThatExceptionOfType(InvalidPhonenumberException::class.java).isThrownBy {
            phonenumberService.parseAndValidate(testData.value)
        }
    }
}